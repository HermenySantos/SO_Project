#ifndef Prompt_C_INCLUDED
#define Prompt_C_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "gestorprocessos.h"


void promptStart(Prompt *c, int escalonamento, int comando){
    (c->gestor).readPipe = (c->writePipe);
    (c->gestor).writePipe = (c->readPipe);
    pipe(c->readPipe);
    pipe(c->writePipe);

    pid_t pid = fork(); 

    if (pid == -1){
        return;
    }

    if (pid > 0) {

        close(c->readPipe[1]); 
        close(c->writePipe[0]); 
        getInput(c, comando);
        
    }
    
    else {
        gestaoProcessos(&(c->gestor), escalonamento);
    }    
}

void getInput(Prompt *c, int comando){
    char instrucao;
    int buffer = 0;
    if(comando==1){
        while (buffer != -1){
            buffer = 0;
            while (buffer == 0){
                fflush( stdout );
                printf("->");
                fflush( stdout );
                scanf("%s", &instrucao);
                buffer = run(c, instrucao);
                if (buffer == 0 )
                    printf("\nInstrução inválida!\n");
                    fflush( stdout );
            }
        }
           
    }
    else if(comando==2){    
    FILE *fp=fopen("control.txt", "r");
    if(fp==NULL){printf("ERROR: Failed to open file - file does not exist\n");exit(1);}   
        while (1){
            fflush(stdout);
            buffer = 0;
            while (buffer == 0){ 
            while (fscanf(fp, "%c\n", &instrucao) != EOF){
                fflush(stdout);
                sleep(1);
                printf("\n->  ");
                fflush(stdout);
                printf("%c", instrucao);
                printf("\n");
            
                buffer = run(c, instrucao);
                if (buffer == 0 ){
                    printf("\nInstrução inválida!\n");
                    fflush( stdout );
                }
            }

            }
    
    sleep(1);
   
    exit(0);
    }
    }
    
    sleep(1);

}

int run(Prompt *c, char comando) {
    
    int num = 0;
    switch(toupper(comando)){

        case 'E': 
            num = CMDE;
            write(c->writePipe[1], &num, sizeof(num)); 
            break;

        case 'I': 
            num = CMDI;
            write(c->writePipe[1], &num, sizeof(num));
            break;
        
        case 'D': 
            num = CMDD;
            write(c->writePipe[1], &num, sizeof(num)); 
            break;
        
        case 'R':  
            num = CMDR;
            write(c->writePipe[1], &num, sizeof(num));
            break;
        
        case 'S':  
            num = CMDS;
            write(c->writePipe[1], &num, sizeof(num));
            break;

        case 'M':  
            num = CMDM;
            write(c->writePipe[1], &num, sizeof(num));
            fflush(stdout);
            break;

        case 'T': 
            num = CMDT;
            write(c->writePipe[1], &num, sizeof(num));
            return -1;

        default: return 0;
    }
    return 1;
}

int tipoLeitura(){
    int comandos;
    do{
        
        printf("\nSelecione uma opção:\n");
        printf("Introduzir instruções pelo teclado 1\n");
        printf("Introduzir Instruções por ficheiros 2\n");
        printf("->");
        
        scanf("%d", &comandos);
        if (comandos < 1 || comandos > 2){
            printf("Opção Inválida!\n");
            comandos = 0;
        }
         
    }
    while(comandos < 1 || comandos > 2);

    return comandos;
}

int tipoEscalonamento(){
    int escalonamento;
    do{
        printf("\nSelecione o tipo de Escalonamento(Por padrão escalonamento =1)\n");
        printf(" Fcfs - 1\n");
        printf(" Prioridade - 2\n");
        printf(" Sjf - 3\n");
        
        printf("->");
        scanf("%d", &escalonamento);
        if ((int)escalonamento < 1 ||(int)escalonamento > 4){
            printf("Opção Inválida!\n");
            escalonamento = 1;
        }
    }
    while((int)escalonamento < 1 || (int)escalonamento > 7);

return escalonamento;
}
int main()
{
    Prompt prompt;
    promptStart(&prompt, tipoEscalonamento(), tipoLeitura());

return 0;
}

#endif 