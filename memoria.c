#include "memoria.h"

Memoria *head;

int primeiravez = 0;

void inicializaMemoria(){

	head = criaMemoria();
}

int allocate_firstFit(int process_id, int num_units) {

	int blocosNecessarios = (int)ceil(num_units/2);

	Memoria *atual = head;
	Memoria *block_start = NULL;

	int blocosLivres = 0;
	int count = 0;
	

	while(blocosLivres < blocosNecessarios) {

		if(atual->memoria == 2) {
			if(blocosLivres == 0) block_start = atual;
			blocosLivres++;
		} 
		
		else blocosLivres = 0;
		count++;
		atual = atual->prox;

		if(atual == NULL)
			if(blocosLivres < blocosNecessarios)
				return -1;
			else break; 
	}
	
	int i;
	atual = block_start;
	for(i = 0; i < blocosNecessarios-1; i++) {
		atual->memoria = 0;
		atual->pid = process_id;
		atual = atual->prox;
	}
	
	atual->memoria = num_units%2;
	atual->pid = process_id;
	
	return count;
}

int allocate_bestFit(int process_id, int num_units) {
	int blocosNecessarios = (int)ceil(num_units/2);
	int blocosLivres = 0;
	int best_block = 256; 
	int count = 0;
	Memoria *atual = head;
	Memoria *block_start = NULL;
	Memoria *best_start = NULL; 
	
	while(atual != NULL) {
		
		if(atual->memoria == 2) {
			if(blocosLivres == 0) block_start = atual;
			blocosLivres++;
		} 
		else blocosLivres = 0;
		
		if((blocosLivres >= blocosNecessarios) && (blocosLivres < best_block)) {
			best_block = blocosLivres;
			best_start = block_start;
		}
		
		count++;
		atual = atual->prox;
		
	}
	
	if(best_start == NULL)
		return -1; 
	
	int i;
	atual = best_start;
	for(i = 0; i < blocosNecessarios-1; i++) {
		atual->memoria = 0;
		atual->pid = process_id;
		atual = atual->prox;
	}
	
	atual->memoria = num_units%2;
	atual->pid = process_id;
	
	return count;
}

int allocate_nextFit(int process_id, int num_units) {
	int blocosNecessarios = (int)ceil(num_units/2);

	Memoria *atual = head;
	Memoria *block_start = NULL;

	int blocosLivres = 0;
	int count = 0;
	

	while(blocosLivres < blocosNecessarios) {

		if(block_start != NULL){
			if(atual->memoria == 2)
				blocosLivres++;

			else { 
			blocosLivres = 0;
			block_start = NULL;
			}
			} 

	    if(block_start == NULL){
			if(primeiravez == 0){
				block_start = atual;
			}
			if(atual->memoria != 2 && atual->prox->memoria == 2) {
			if(blocosLivres == 0) 
			block_start = atual->prox;
		} 
		}
		count++;
		atual = atual->prox;

		if(atual == NULL)
			if(blocosLivres < blocosNecessarios)
				return -1;
			else break; 
	}
	
	int i;
	atual = block_start;
	for(i = 0; i < blocosNecessarios-1; i++) {
		atual->memoria = 0;
		atual->pid = process_id;
		atual = atual->prox;
	}
	
	atual->memoria = num_units%2;
	atual->pid = process_id;
	
	return count;
}

int allocate_worstFit(int process_id, int num_units) {
	int blocosNecessarios = (int)ceil(num_units/2);
	int blocosLivres = 0;
	int worst_block = 0; 
	int count = 0;
	Memoria *atual = head;
	Memoria *block_start = NULL;
	Memoria *worst_start = NULL; 
	
	while(atual != NULL) {

		if(atual->memoria == 2) {

			if(blocosLivres == 0)
				block_start = atual;
			
			blocosLivres++;
		} 
		else blocosLivres = 0;
		
		if((blocosLivres >= blocosNecessarios) && (blocosLivres > worst_block)) {
			worst_block = blocosLivres;
			worst_start = block_start;
		}
		
		count++;
		atual = atual->prox;
	}
	
	if(worst_start == NULL)
		return -1; 
	
	int i;
	atual = worst_start;
	for(i = 0; i < blocosNecessarios-1; i++) {
		atual->memoria = 0;
		atual->pid = process_id;
		atual = atual->prox;
	}

	atual->memoria = num_units%2;
	atual->pid = process_id;
	
	return count;
}

int fragment_count() {
	int count = 0;
	Memoria *atual = head;
	while(atual->prox != NULL) 
    {
		count += atual->memoria;
		atual = atual->prox;
	}
	return count;
}

int deallocate_mem(int process_id) {
	Memoria *atual = head;
	int alocado = -1;
	while(atual->prox != NULL) {
		if(atual->pid == process_id) {
			atual->pid = 0;
			atual->memoria = 2;
			alocado = 1;
		} 
        else if(alocado == 1) break;
		
		atual = atual->prox;
	}
	return alocado;
}

void simula(int tipo) {

	head = criaMemoria();
	int i, cruzado = 0, acertos = 0, resultado = 0;

	Memoria *pIdHead = (Memoria *)malloc(sizeof(Memoria)); 
	Memoria *idAtual = pIdHead;
	idAtual->prox = pIdHead;
	srand(time(0));  
  
	for(i = 0; i < 10000; i++) {
		int pid = rand() % 10000;

            if(rand() % 3 == 1) 
            {
                deallocate_mem(pIdHead->prox->pid);
	            pIdHead->prox = pIdHead->prox->prox;
            }
            
			     resultado = allocate_firstFit(pid, (3+rand()%8));
            /*else if (tipo == 1)
			     resultado = allocate_nextFit(pid, (3+rand()%8));
            else if (tipo == 2)
			     resultado = allocate_bestFit(pid, (3+rand()%8));
            else if (tipo == 3)
			     resultado = allocate_worstFit(pid, (3+rand()%8));*/

            if (resultado != -1) {
				idAtual->pid = pid;
				idAtual->prox = (Memoria *) malloc(sizeof(Memoria));
				idAtual = idAtual->prox;
				idAtual->prox = pIdHead;
				acertos++;
				cruzado += resultado;
			}
            /*else {
                deallocate_mem(pIdHead->prox->pid); 
	            pIdHead->prox = pIdHead->prox->prox;
            }*/
		}
	
	printf("Tempo medio nós atravessados: %.2f\n",(double)(cruzado/acertos));
	printf("Solicitacoes negadas: %.2f\n",(double)((10000-acertos)/10000*100));
	printf("número médio de fragmentos externos: %.2f\n",(double)(fragment_count()/acertos));

}

void reportMemoria() {
	Memoria *atual = head;
	int i = 1;
    printf("\n__MEMÓRIA__\n\n");
	while(atual->prox != NULL) {
        if(atual->pid == 0) printf("%3c", 'X' );
        else printf("%3d", atual->pid);
		atual = atual->prox;
		if (i % 16 == 0) printf("\n"); i++;
	}
	printf("\n");
}

Memoria *criaMemoria() {
	Memoria *head = (Memoria *)malloc(sizeof(Memoria));
	Memoria *atual = head;

	for(int i = 0; i < 256; i++) {
		atual->memoria = 2;
		atual->pid = 0;
		atual->prox = (Memoria *)malloc(sizeof(Memoria));
		atual = atual->prox;
	}
	
	return head;
}