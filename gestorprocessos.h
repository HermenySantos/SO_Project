#ifndef GESTORPROCESSOS_H_INCLUDED
#define GESTORPROCESSOS_H_INCLUDED

#include "processo.h"
#include "cpu.h"
#include "processo.h"

#define CMDE 1 //COMANDO 'E'
#define CMDI 2 //COMANDO 'E'
#define CMDR 3 //COMANDO 'E'
#define CMDT 4 //COMANDO 'E'
#define CMDD 5 //COMANDO 'D'
#define CMDM 6 //COMANDO 'D'
#define CMDS 7 //COMANDO 'D'

#define MAXPROCESS 100
#define BUFFERSIZE 20000

//determinar o tipo de escalonamento
#define FCFS 1
#define SJFS 1
#define PRIORITY 1
#define LONGOPRAZO 1
#define RATEMONOTONIC 1
#define EDF 1

typedef struct Gestor{
    int *readPipe;
    int *writePipe;
    int pipein[2];
    int pipeout[2];
    int time;
    Cpu cpu;
    PCB* pcbTabela[100];
    TLista pProntos;
    TLista pWaiting;
    TLista pBloqueados;
    TLista pTerminados;
    TLista pFilhos;
    int pRunning;
    int contadorID;
    int ultimoExecutado;
    int tempoMedio;
    int qtdProcessosEncerrados;
} Gestor;

typedef struct Prompt{
    int readPipe[5];
    int writePipe[5];
    Gestor gestor;
    //gestor = malloc(sizeof(Gestor)*1001);
}Prompt;

void gestaoProcessos(Gestor *g, int escalonamento);
void recebeComandosGestor(Gestor *g, int escalonamento);
void inicializaEstruturas(Gestor *g);
int criaProcesso(Gestor *g, int id, int prioridade, char *file, int arrivalTime);
int criaProcessoFilho(Gestor *g, PCB *pai);
void comutacaoContexto(Gestor *g, int quantum, int processonovo, int filho);
int escalonar(Gestor *g, int tipoEscalonamento);
void promptStart(Prompt *c, int escalonamento, int comando);
void getInput(Prompt *c, int comando);
int run(Prompt *c, char comando);

#endif 
