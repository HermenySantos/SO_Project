#ifndef PROCESSO_H_INCLUDED
#define PROCESSO_H_INCLUDED


#include <stdio.h>
#include <stdlib.h>

#define TAMANHOT 100

//ESTADOS PROCESSOS
#define NOVO 0
#define BLOQUEADO 1
#define PRONTO 2
#define RUNNING 3
#define WAITING 4
#define TERMINADO -1


typedef struct Processo {
    int indiceProcesso;
    int pid;
    int ppid;
    int prioridade;
    int startTime;
    int burstTime;
    int variavel;
    int finishTime;
    int waitingTime;
    int turnaroundTime;
    int arrivalTime;
    int periodo;
    int deadline;
    //int responseTime;
    int memoriaAlocada;
} Processo;


typedef struct node* NodePointer;
typedef struct node {
    Processo Item;
    struct node* pProx; 
} Node;


typedef struct {
    struct node* pPrimeiro;
    struct node* pUltimo;
    int Contador;
} TLista;

typedef struct {
    char instrucao;
    int valor;
    char nome [15];
}Instrucao;

typedef struct PCB{
    //processo
    char nome[15];
    int id; 
    int idPai;
    //operacoes
    int pc; 
    int variavel; 
    int prioridade;
    int estadoAtual;
    int arrivalTime; //arrivalTime
    //adicionei depois
    int burstTime; //igual a quantidade de instrucoes ou duracao
    int waitingTime;
    int turnaroundTime;
    //####estatisticas
    int startTime;
    int finishTime;
    int timeRunning;     
 
    //fim estatisticas
    char texto[TAMANHOT][20]; 
    int qtdInst; 

    //adicionado extra para escalonamento tempo real
    int deadline;
    int periodo;

    int memoriaAlocada;

}PCB;

void createFila(TLista* listProc);
int isEmpty(TLista* listProc);
void addElemento(TLista *listProc, Processo* proc);
int removeElemento(TLista* listProc, int index);

#endif 
