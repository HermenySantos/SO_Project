#A Simple Example Makefile for soshell
#
# Syntax:
# Alvo: Dependencias
# tab Action 1
# tab Action 2
#
# Variaveis: Compiler, compiler flags, libraries to link, name of of object files
#
CC=cc
FLAGS=-c -Wall -w
LIBS=-lm
LIBS2=-lm -lpthread
OBS=main.o execute.o parse.o
OBS2= cpu.o core.o door.o gestorprocessos.o escalonador.o estatisticas.o memoria.o
 
#Alvo por defeito é o primeiro
all :  soWork projeto

main.o : shell.h main.c
	$(CC) $(FLAGS) main.c
execute.o : shell.h execute.c
	$(CC) $(FLAGS) execute.c
parse.o : shell.h parse.c
	$(CC) $(FLAGS) parse.c
cpu.o : cpu.h cpu.c
	$(CC) $(FLAGS) cpu.c
door.o : gestorprocessos.h door.c
	$(CC) $(FLAGS) door.c
gestorprocessos.o : gestorprocessos.h gestorprocessos.c
	$(CC) $(FLAGS) gestorprocessos.c
core.o	: processo.h core.c
	$(CC) $(FLAGS) core.c
escalonador.o: escalonador.h escalonador.c
	$(CC) $(FLAGS) escalonador.c
estatisticas.o: estatisticas.h estatisticas.c
	$(CC) $(FLAGS) estatisticas.c

memoria.o: memoria.h memoria.c
	$(CC) $(FLAGS) memoria.c

projeto : $(OBS2)
	$(CC)  -o projeto $(OBS2) $(LIBS2)

soWork : $(OBS)
	$(CC)  -o soWork $(OBS) $(LIBS)

clean limpar:
	rm -f soWork projeto *.o
	rm -f *~
remake:	
	rm -f soWork projeto *.o
	rm -f *~
	make all