#include "escalonador.h"
#include "gestorprocessos.h"
void fcfs(Gestor *g);
void priority(Gestor *g);
void stcf(Gestor *g);
void bjf(Gestor *g);
void execFilho(Gestor *g);


int escalonar(Gestor *g, int tipoEscalonamento){ 

    int processoEscolhido =-1;
    int i, j, prioridade, quantum, primeiro, ultimo;
    primeiro = -1;
    ultimo = -1;
    char b[30];
    if(g->pRunning != -1){
        switch (g->pcbTabela[g->pRunning]->estadoAtual) {
            case RUNNING: 
                printf("processo a ser executado\n");
                //insereLista(g, g->pRunning, 'P');
                break;

            case WAITING: 
                g->ultimoExecutado = g->pRunning;
                insereLista(g, g->pRunning, 'W' );
                break;
           
            case BLOQUEADO: //se foi bloqueado
                g->ultimoExecutado = g->pRunning;
                insereLista(g, g->pRunning, 'B');
                break;

             case TERMINADO: //se terminou execuçao
                printf("\no processo  terminado\n");
                deallocate_mem(g->pRunning);
                insereLista(g, g->pRunning, 'T');
                g->qtdProcessosEncerrados++;
                free(g->pcbTabela[g->pRunning]);
                g->pcbTabela[g->pRunning] = NULL;
                g->pRunning = -1;
                break;

            default : 
            break;
        }
    }

        switch(tipoEscalonamento){
            case 1 : fcfs(g); break;
            case 2 : priority(g); break;
            case 3 : stcf(g); break;
            
        }
        
}


void fcfs(Gestor *g){

    int i, processoEscolhido = -1;
    NodePointer aux, aux1;
    int menorTempo = 10000;

    if((g->pProntos.pPrimeiro->pProx != NULL) && (g->pProntos.pPrimeiro->pProx->Item.startTime-1 <= g->time)){
    
        for(int j=g->time; j>=0; j--){ 
            aux = g->pProntos.pPrimeiro->pProx;
            while(aux != NULL) {
                if(menorTempo > aux->Item.startTime)
                {
                    menorTempo = aux->Item.startTime;
                    processoEscolhido = aux->Item.indiceProcesso;
                }
                aux = aux->pProx;
            }
        }
        if(processoEscolhido != -1) 
        comutacaoContexto(g, g->pcbTabela[processoEscolhido]->qtdInst, processoEscolhido, 0);
    }
    else comutacaoContexto(g, 10, processoEscolhido, 0);
}

void priority(Gestor *g){

    int i, processoEscolhido = -1;
    NodePointer aux, aux1;
    int maiorPrioridade = 10000;

    if((g->pProntos.pPrimeiro->pProx != NULL) && (g->pProntos.pPrimeiro->pProx->Item.startTime-1 <= g->time)){
       
        for(int j=99; j>=0; j--){ 
            aux = g->pProntos.pPrimeiro->pProx;
            while(aux != NULL) {
                // i = aux->Item.indiceProcesso;
                //maiorPrioridade = aux->Item.prioridade;
                if(maiorPrioridade > aux->Item.prioridade)
                {
                    maiorPrioridade = aux->Item.prioridade;
                    processoEscolhido = aux->Item.indiceProcesso;
                }
                aux = aux->pProx;
            }
            
        }

        if(processoEscolhido != -1) 
        comutacaoContexto(g, g->pcbTabela[processoEscolhido]->qtdInst, processoEscolhido, 0);
    }
    else comutacaoContexto(g, 10, processoEscolhido, 0);
}

void stcf(Gestor *g){
    int i, processoEscolhido = -1;
    NodePointer aux, aux1;
    int menorTempo = 10000;

    if((g->pProntos.pPrimeiro->pProx != NULL) && (g->pProntos.pPrimeiro->pProx->Item.startTime-1 <= g->time)){
     
        for(int j=g->time; j>=0; j--){ 
            aux = g->pProntos.pPrimeiro->pProx;
            while(aux != NULL) {
                if(menorTempo > aux->Item.burstTime)
                {
                    menorTempo = aux->Item.burstTime;
                    processoEscolhido = aux->Item.indiceProcesso;
                }
                aux = aux->pProx;
            }
        }
        if(processoEscolhido != -1) 
        comutacaoContexto(g, g->pcbTabela[processoEscolhido]->qtdInst, processoEscolhido, 0);
    }
    else comutacaoContexto(g, 10, processoEscolhido, 0);
}

void bjf(Gestor *g){

    int i, processoEscolhido = -1;
    NodePointer aux, aux1;
    int maior = 0;

    if((g->pProntos.pPrimeiro->pProx != NULL) && (g->pProntos.pPrimeiro->pProx->Item.startTime-1 <= g->time)){
    
        for(int j=g->time; j>=0; j--){ 
            aux = g->pProntos.pPrimeiro->pProx;
            while(aux != NULL) {
                if(maior < aux->Item.burstTime)
                {
                    maior = aux->Item.burstTime;
                    processoEscolhido = aux->Item.indiceProcesso;
                }
                aux = aux->pProx;
            }
        }

        if(processoEscolhido != -1) 
        comutacaoContexto(g, g->pcbTabela[processoEscolhido]->qtdInst, processoEscolhido, 0);
    }
    else comutacaoContexto(g, 10, processoEscolhido, 0);
}

void execFilho(Gestor *g){
    int nextFilho = -1;

    if((g->pFilhos.pPrimeiro->pProx != NULL)){
            nextFilho = g->pFilhos.pPrimeiro->pProx->Item.indiceProcesso;
            //removeElemento(&g->pFilhos, nextFilho);

        comutacaoContexto(g, g->pcbTabela[nextFilho]->qtdInst, nextFilho, 1);
    }
    else comutacaoContexto(g, 10, nextFilho, 2);
}

void voltaPai(Gestor *g){
    comutacaoContexto(g, 10, g->ultimoExecutado, 2);
}

void ratemonotonic(Gestor *g){
}

void edf(Gestor *g){
}

int longoPrazo(Gestor *g){

    NodePointer aux;
    int menorStart = 1000;
    int indiceProcesso = -1;
    aux = g->pBloqueados.pPrimeiro->pProx;
    while(aux != NULL){

        if (menorStart > aux->Item.startTime)
            {
                menorStart = aux->Item.startTime;
                indiceProcesso = aux->Item.indiceProcesso;
            }

            aux = aux->pProx;
    }

    return indiceProcesso;
}
/*void bestjobfirst(Gestor *g){

    int i, processoEscolhido = -1;
    NodePointer aux;
    int bestJob = 1000;
    int bestEstatisticas = 0;
    if((g->pProntos.pPrimeiro->pProx != NULL) && (g->pProntos.pPrimeiro->pProx->Item.startTime-1 <= g->time)){

            aux = g->pProntos.pPrimeiro->pProx;
            while(aux != NULL) {
                bestEstatisticas =  aux->Item.prioridade + aux->Item.arrivalTime + aux->Item.burstTime;

                if(bestJob > bestEstatisticas)
                {
                    bestJob = bestEstatisticas;
                    processoEscolhido = aux->Item.indiceProcesso;
                }
                aux = aux->pProx;
            }

        if(processoEscolhido != -1) 
        comutacaoContexto(g, g->pcbTabela[processoEscolhido]->qtdInst, processoEscolhido, 0);
    }
    else comutacaoContexto(g, 10, processoEscolhido, 0);
}
*/