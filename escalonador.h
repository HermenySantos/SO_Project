#ifndef ESCALONADOR_H_INCLUDED
#define ESCALONADOR_H_INCLUDED


#include <stdio.h>
#include <stdlib.h>
#include "gestorprocessos.h"

int escalonar(Gestor *g, int tipoEscalonamento);

#endif
