#include "processo.h"
#include <string.h>

int carregaInstrucoes(PCB *p, char *ficheiro) 
{
    p->qtdInst = 0;
    FILE *file;
    file = fopen(ficheiro,"r");
    char c[30];
    int i = 0;

    if(file == NULL){
        printf("Erro ao abrir o ficheiro.\n");
        return 0;
    }
    else
    {
        while( (fgets(c, sizeof(c), file))!=NULL ){
            for(i=0; i<10; i++){
                if(c[i] == '\n'){
                    c[i] = '\0';
                    break;
                }
            }
            strcpy(p->texto[p->qtdInst], c);
            
                p->qtdInst++;
            
            
        }
        p->qtdInst--;
    }
    fclose(file);
    return 1;
}


void createFila(TLista* listProc) {

    listProc->pPrimeiro = (NodePointer) malloc(sizeof(Node));
    listProc->pUltimo = listProc->pPrimeiro;
    listProc->pPrimeiro->pProx = NULL;
    listProc->Contador=0;
}

int isEmpty(TLista* listProc) {
    return (listProc->pPrimeiro == listProc->pUltimo);
}

void addElemento(TLista *listProc,Processo* proc) {
    
    listProc->pUltimo->pProx = (NodePointer) malloc(sizeof(Node));
    listProc->pUltimo = listProc->pUltimo->pProx;
    listProc->pUltimo->Item = *proc;
    listProc->pUltimo->pProx = NULL;
    listProc->Contador++;
    
}

int removeElemento (TLista* listProc, int index) {
    NodePointer aux, proximo;
    
    if (isEmpty(listProc)) return 0; 
    aux = listProc->pPrimeiro;

    while(aux != NULL) {
        if(aux->pProx->Item.indiceProcesso == index) {
            proximo = aux->pProx->pProx;
            if(proximo == NULL) {
                listProc->pUltimo = aux;
            }

            free(aux->pProx);
            aux->pProx = proximo;
            listProc->Contador--;
            return 1;
        }
        aux = aux->pProx;
    }
    return 0;
}


