typedef struct {
     char ins; 
     int n; 
     char nome[30] ;
     } instruction;


typedef struct x
{   int exist; //flag que é -1 se processo não existe e 1 es existe
    int val;//variavel a ser alterada no processo depois de ter alterado a variavel global
    int count;// contador de quantas vezes o processo ainda va executar
    int priority;// prioridade
    int Estado;//5 estados novo - 1, pronto -2, executando -3, em espera ou bloqueado -4, terminado -5
    int PID;//id do processo propriamente dito de 0- 1000
    int PPID;//id do processo pai que o gerou
    char* Program;//nome do programa
    int Start;// ponto de inicio do processo 
    int PC ;// identifica o proximo processo a ser executado
    int espera; //tempo que esperou ate executar
    int fim;
    //int duracao; tempo de duração do process
    struct x * next;
    struct x * prev;
    
}PCB;

/*ZONA DAS GLOBAL VAR*/
int Tempo=0,tempo=0;//flag time,timer
int p=-1;
instruction memory [1000];

PCB *PList;


char prompt[100];
int numargs, GlobalVal;

//prototipo
int validlength();
int gerarNumeroInteiro (int a, int b);
void pri(int s);
void ReadPCB( char *fl ,int TAM, int start);
void initPCB(PCB *P, int TAM, int c, int start);
int PCBcount(PCB *P);
void creatPlan();
void processa( PCB *ind, int *i);
void corre(PCB * L, int *i);
void changeState();
void executar(char *escal);
PCB * infoReady(PCB * L);
PCB * infoBlocker(PCB * L);
PCB * infoTerminated(PCB * L);
void priPCB();
PCB * createHead();
void fcfs (PCB *proc);
void sjf (PCB *proc);
void prioridade (PCB *proc);

void Reportage2(PCB *L, int tip);
//void swap ( int* a, int* b );
PCB *lastProc(PCB *L);
//void quickSort(PCB *head);
//void _quickSort(PCB *l, PCB *h);
//PCB * partition(PCB *l, PCB *h);
PCB * CopyPCB(PCB *L1, PCB *L2);
PCB * addLast(PCB * L, PCB * novo);
PCB * listErase(PCB * L);
PCB * goHead(PCB * L);
/*terminamos até a pag 3 o proximo será a pag 4
*/

int builtin (char **args){//AQUI
  
    if(numargs == 1 && strcmp(args [0], "start")==0|| numargs == 1 && strcmp(args [0], "começa")==0 ){// analiza parametros
  
    args[0]="./projeto";
  
      return 0;
      }
    if(numargs == 2 && strcmp(args [0], "M")==0){// analiza parametros
  
    //guarda em memory
    /*falta fazer printf da memory pra saber se guardou bem.M*/

    meme (args [0], args [1], NULL,0);
    
      return 1;
        // chama a função M n - mudar o valor da variável para o valor n
    }
    if(numargs == 2 && strcmp(args [0], "A")==0){
         
         meme (args [0], args [1], NULL,0);
          
         
         return 1;
        // // chama a função A n - adicionar o valor n à variável
    }
    if(numargs == 2 && strcmp(args [0], "S")==0){
         meme (args [0], args [1], NULL,0);
          
         return 1;
        // chama a função S n - subtrair
    }
    if(numargs == 2 && strcmp(args [0], "C")==0){
         meme (args [0], args [1], NULL,0);
       
         return 1;
        // chama a função C n - Criar um novo processo (cópia do velho) - o processo filho executa
        // logo a seguir esta instrução enquanto o pai executa n instruções a partir desta.
    }
    if(numargs == 2 && strcmp(args [0], "L")==0){
         meme (args [0], NULL ,args[1],1);
        
         return 1;
        // chama a função L filename - Limpar o programa atual e substituir 
        //por filename (max 15 caracteres) Terá que carregar o novo programa em memória se for necessário.
    }

    if(numargs == 1 && strcmp(args [0], "T")==0){
        //Terminar este processo
       meme (args [0],args [1], "Terminar",1);
        
       return 1;
    }
    
    if(numargs == 1 && strcmp(args [0], "B")==0){
        //Bloquear este processo
        meme (args [0], args [1], "Bloquear",1);
        
        return 1;
    }
    if(numargs == 2 && strcmp(args [0], "E")==0){
        //executar com escalonador escolhido
        pri(p);
        printf("digite o tempo maximo de processamento em segundos: \n");
        scanf("%i",&Tempo);
        executar (args [1]);
        return 1;
    }
    if(numargs == 1 && strcmp(args [0], "E")==0){
        //executar com escalonador escolhido
        pri(p);
        printf("digite o tempo maximo de processamento em segundos: \n");
        scanf("%i",&Tempo);
        printf("\tfunc exec entrou\n"); 
        executar ("fcfs");
        printf("\tfunc exec saiu\n"); 
        return 1;
    }
    if(numargs == 2 && ((strcmp(args [0], "report")==0 && strcmp(args [1], "G")==0) || (strcmp(args [0], "R")==0 && strcmp(args [1], "g")==0))){
       
         Reportage2(PList,2);
        return 1;
    }
    if(numargs == 2 && ((strcmp(args [0], "report")==0 && strcmp(args [1], "L")==0) || (strcmp(args [0], "R")==0 && strcmp(args [1], "l")==0))){
      
        Reportage2(PList,1);
        return 1;
    }
    if(numargs == 1 && strcmp(args [0], "R")==0){
        //executar com escalonador escolhido
         meme (args [0], args [1], "Reportar local",1);
        return 1;
    }
    
    
    if(numargs == 4 && strcmp(args [0], "plan")==0 && args [2]>=0 && args [3]>0 ){
        ReadPCB(args[1],atoi(args[3]),atoi(args[2]));
        printf("%i processos novos, chegam em %i segs\n",atoi(args[3]),atoi(args[2]));
        
        //falta imprimir PCB pra saber se leu do ficheiro
        return 1;
    }
  
    if (strcmp (args[0], "sair") == 0)
    {
        exit (0);
        return 1;
    }
        return 0;
    }

void meme (char* ins, char* n, char* nome, int f){
    p++;
    if(f==0){//se n recebe nome
        memory[p].ins= ins[0]; // recebe a instrucao ou caracter  guarda no vetor da memoria 
        memory[p].n= atoi(n); // converter string em inteiro recebido nas instruçoes do teclado 
       
        }
        else{
        memory[p].ins= ins[0]; 
        memory[p].n= 0; 
       // memory[p].nome=nome;
        strcpy(memory[p].nome, nome); // copia o nome no vetor memory , nome dado n parametro
        
       }
    }
 
void pri( int s){ // imprir tudo que esta na memoria
 printf("instructions list review\n");
    int i=0;
        do{
                printf("instruction %c num = %i nome= %s \n",memory[i].ins ,memory[i].n ,memory[i].nome);
                i++;
        }while(i<=s);
    
    }

int PCBcount(PCB *P){
    
    int x=0;
    while(P!=NULL){
        if(P->exist==1)
        x++; 
      P=P->next;
    }
    return x;
    }



void ReadPCB(char *fl ,int TAM, int start){
    
    PCB *P=NULL;
    int c = PCBcount(PList);// c diz qunatos espaços estao ocupado no nosso vetor de 1000
    //PCB counta o numero de processo que estão na memoria
    //Le o ficheiro criado e coloca no array Arraypcb
    FILE * fp;
    fp = fopen(fl , "rb");
    
    //for (int i = 0; i < TAM; i++)
    // fread(P, sizeof(PCB),1, fp);
    //trabalhar com fseek 
    //priPCB();
    initPCB(P, TAM, c, start);
        fclose(fp);
        free(P);
    
    }

void priPCB(){
    int c=PCBcount(PList);
    PCB* L=PList;
    for (int i = 0; i < c; i++) {
        printf("Prog_nome = %s\n", L->Program);
        printf("Prioridade = %d\n", L->priority);
        printf("Inicio = %d\n", L->Start);
        printf("Valor = %d\n", L->val);
        printf("conta = %d\n", L->count);
        printf("PID = %d\n", L->PID);
        printf("PPID = %d\n", L->PPID);
        printf("PC = %d\n\n\n", L->PC);
        printf("Estado = %d\n\n\n", L->Estado);
        L=L->next;
    }


    }

PCB * infoBlocker(PCB *L){
   
    L=goHead(L);
    PCB * B;
    if(L!=NULL){
    do{
        if ( L->exist == 1 &&  L->Estado== 4){
            
            B= addLast(B,CopyPCB(createHead(),L)); 
            
        }
        if( L->exist!=1){
           
            break;
        }
        L=L->next;
    }while(L->next!=NULL);}
    else return L;
    printf("Blocked returned\n");
    return B;
    }

PCB* infoReady(PCB *L){
L=goHead(L);
    PCB *R;
    if(L!=NULL){
        do{
            if( L->exist == 1 &&  L->Estado== 2){
                R= addLast(R,CopyPCB(createHead(),L)); 
            }
            if( L->exist!=1){ 
                break;
            }
            L=L->next;
        }while(L->next!=NULL); }
    else return L;

    printf("readys returned\n");
    return R;
    }

PCB * infoTerminated(PCB *L){ 
    
    L=goHead(L);
    PCB *T;
    if(L!=NULL){
        do{
            
            if ( L->exist == 1 &&  L->Estado== 5){
                T= addLast(T,CopyPCB(createHead(),L));  
            }
            if( L->exist!=1){ 
                break; 
            }
            L=L->next;
    }while(L->next!=NULL);}
    else return L;

    printf("terminated returned\n");
    return T;
}
    
    

void executar(char *escal){
 printf("\texecutando\n"); 


       //changeState();//muda o estado dos processos novos pra ready
    if (strcmp(escal,"sj")==0 || strcmp(escal,"sjfc")==0 ||strcmp(escal,"SJ")==0 || strcmp(escal,"SJFC")==0 )
       {
              sjf(PList);
        }
       else if(  strcmp(escal,"Pri")==0 || strcmp(escal,"pri")==0 ||  strcmp(escal,"PRI")==0 ||  strcmp(escal,"prio")==0  ){
              prioridade (PList);
       }
       else{ //printf("\tfunc fcfs entrou\n"); 
              fcfs (PList);
              //printf("\tfunc fcfs saiu\n"); 
       }

       
    }
   
void changeState(){
     
    PCB * aux=goHead(PList);
     
    while(aux!=NULL){
    if (tempo==aux->Start && aux->Estado == 1 && aux->exist== 1)
            aux->Estado=2;
      aux=aux->next;
       }
} 

void corre(PCB * L , int *i){  
    processa(L,i);       
              if(L->count==0 && L->Estado==3){                
                    L->Estado=5;// terminado       
                }
    changeState();
    }

    
    
void processa(PCB *L, int *j){
    int maxind=p;
    
        L->Estado=3;
        


    if(memory[*j].ins=='M'){
        printf("\tinst M\n");  
        GlobalVal=memory[*j].n;
        L->val=GlobalVal;
        if(L->next!=NULL) 
            L->PC=L->next->PID;
         
    }
    if(memory[*j].ins=='S'){
        printf("\tinst S\n"); 
        GlobalVal -=memory[*j].n; 
         
        L->val=GlobalVal;
        if(L->next!=NULL)  
            L->PC=L->next->PID;
        //printf("\tfim S\n"); 
    }if(memory[*j].ins=='L'){
        printf("\tinst L\n"); 
        FILE * g;
        g=fopen(memory[*j].nome, "rb");
        PCB *aux= (PCB *) malloc(sizeof(PCB )*1);
        fread(aux,sizeof(PCB),1,g);
        L=CopyPCB(L,aux);
        
        L->val=GlobalVal;
        if(L->next!=NULL) 
            L->PC=L->next->PID;
        free(aux);
        fclose(g);
        //printf("\tfim L\n");  

    }if(memory[*j].ins=='B'){
        printf("\tinst B\n"); 
        L->Estado=4; 
        if(L->next!=NULL) 
        L->PC=L->next->PID;
        //printf("\tfim B\n"); 
    }if(memory[*j].ins=='A'){
        printf("\tinst A\n"); 
        GlobalVal +=memory[*j].n;
        L->val=GlobalVal;
        if(L->next!=NULL)  
        L->PC=L->next->PID;
        //printf("\tfim A\n");  

    }if(memory[*j].ins=='C'){
       printf("\tinst C copy\n");
        PCB * filho = createHead();

        filho->exist=L->exist;
        filho->priority=L->priority;
        filho->Estado=L->Estado;
        filho->PID=1010;
        filho->PPID=L->PID;
        strcpy( filho->Program , L->Program);
        filho->Start=L->Start;
        filho->count=L->count;
        filho->val=L->val;
        L->PC=filho->PID;

        filho->next=L->next;
        if(L->next!=NULL) 
        L->next->prev=filho;
        filho->prev=L;
        L->next=filho;
        if(L->next!=NULL) 
        filho->PC=L->next->PID;

       

        //acho devemos usar forks
    }if(memory[*j].ins=='I'){
        printf("processo interrompindo\n");
        //I : Interrompa o processo em execução e bloquei-o
       L->Estado= 4;//blocked

    }if(memory[*j].ins=='R'){
        printf("Reportando\n");
        Reportage2(L,1);
        //Reportagem(1);
    }
    if(memory[*j].ins=='D'){

    }if(memory[*j].ins=='T'){
        printf("Terminando\n");
        L->Estado= 4;
        //Reportage2(PList,2);
        //Reportagem(2);
        exit(1);
    }
    if(*j==maxind){ *j=0;} 

    }
    


    /* Inicialização de entrada da lista de processos*/
PCB *CopyPCB (PCB* L1, PCB* L2) {
    
 if (L2 == NULL) {
        printf("Erro Fatal: Falha Alocacao de memoria.\nFinalizar.\n");
        exit(1);
    }

    L1->exist=L2->exist ;
    L1->count=L2->count ;
    L1->priority = L2-> priority;
    L1->espera = L2->espera ;
    L1->prev = L2->prev  ;
    L1->next=L2->next ;
    L1->val=L2->val ;
    L1->Estado=L2->Estado ;
    L1->Start=L2->Start ;
    L1->PC=L2-> PC;
    L1->PPID=L2->PPID ;
    L1->PID=L2->PID ;
    strcpy(L1->Program, L2->Program) ;

    return(L1);
}

PCB * createHead(){
    PCB *L= (PCB *) malloc(sizeof(PCB ));
    L->exist=-1;
    L->val=0;//random 1-100 
    L->Start=0;
    L->PC=0;// depois de escalonar
    L->count= 0;//random
    L->priority=0;//random 1-10;
    L->Estado=0;
    L->PC=0;
    L->PPID=0;
    L->PID=0;
    L->Program=(char *)malloc(sizeof(char)*15);
    L->next=NULL;
    L->prev=NULL;
    return L;
}

PCB *addLast(PCB * L, PCB * novo){
    if(L==NULL)return novo;
    else
    while(L->next!=NULL){
        L=L->next;
    }
    L->next=novo;
    novo->prev=L;
    return goHead(L);

}

PCB * goHead(PCB *L){
    while(L->prev!=NULL) L=L->prev;
    return L;
}


void initPCB(PCB *P, int TAM, int c, int start){
    PCB *aux= createHead() ;


    for(int i = 0; i < TAM; i++){
        aux->exist=1;
        aux->val=gerarNumeroInteiro (1, 100);//random 1-100 
        aux->count= gerarNumeroInteiro (1, 5);//random
        aux->priority=gerarNumeroInteiro (1, 10);//random 1-10;
        aux->Estado=gerarNumeroInteiro (1, 5);;//Estado de Novo processo do digrm 5 states
        aux->Start=start;
        aux->PC=9 ; // depois de escalonar
        aux->PPID=0;
        aux->PID=c+i+1;
        snprintf(aux->Program,15, "PS_%d", c+i+1);
        P=addLast(P,aux);
        aux=aux->next;
        aux=createHead();
        
    }
       
       PList=addLast(PList,P);
       
       aux=listErase(aux);
     
    }





void fcfs (PCB *proc) {
 
    int inicio, fim, k=0;
    PCB *tmp = proc;
    unsigned int start=1;
  printf("\tEscalonamento FCFS\n");
    
  while (start) {
    inicio = tempo;
    printf("\tWORKING...\n");
    
    for(int i=inicio,j=tempo+tmp->count; i<j;i++){ //i=inicio, j=tempo+tmp->count
        tmp->count--;//2-1-0
       
        tempo++;// 2
        corre(tmp,&k);
         k++;
         
      if(tmp->count==0){
          printf("\tprocessamento terminado\n");
        fim = tempo;
        tmp->fim=fim;
          printf("\tterminado %i segs\n", fim);
        tmp =tmp->next;
         if(tmp==NULL){
             printf("\tI am NULL\n");
             start=0;
             break;
         }
         break;
        }
        
    if(tempo==Tempo){
          printf("\ttempo limite\n");
          fim = Tempo;
          tmp->fim=fim;
          break;
        }
      }
        if(tempo==Tempo){
            
          break;
        } 
  }
    printf("finalizou fcfs\n\n");
    
}


void prioridade (PCB *proc) {
  int inicio, fim, maior,k=0;
  PCB *copia, *tmpsrc, *tmp, *maiorprimeiro;
  printf("\tEscalonamento por Prioridade\n");
   printf("\n");

     /* Replicando Lista de Processos */
  tmpsrc = CopyPCB(createHead(),proc);
  
  copia = tmp = NULL;
  while (tmpsrc != NULL) {
    if (copia == NULL) {
    copia = CopyPCB(createHead(),tmpsrc);
     
    tmp = copia;
     
    } else {
    tmp->next = CopyPCB(createHead(),tmpsrc);
     
    tmp = tmp->next;
    }
     
    tmpsrc = tmpsrc->next;
  }
  /* Programa Principal */
  tempo = 0;
   
  while (copia != NULL) {
 
          /* Localiza o prfoximo processo */
    maiorprimeiro = NULL;  
    maior = copia->priority;
    tmp = copia->next;
    tmpsrc = copia;
            while (tmp != NULL) {
                if (tmp->priority < maior) {
                maior = tmp->priority;
                maiorprimeiro = tmpsrc;
                }
                tmpsrc = tmp;
                tmp = tmp->next;
            
                if (maiorprimeiro == NULL) {
                /* Verifica se o primeiro processo possui maior prioridade */
                    inicio = tempo;
                    printf("\twork for\n");
                
                    for(int i=inicio,j=tempo+copia->count; i<j;i++){
                        copia->count--;
                        tempo++;
                        corre(copia,&k);
                        k++;       
                        if(copia->count==0){
                            fim = tempo;
                            copia->fim=fim;
                            printf("\tterminado %i segs\n", fim);
                            
                                }
                        if(tempo==Tempo){
                            fim = Tempo;
                            copia->fim=fim;
                            printf("\ttempo limite\n");
                            break;
                            }

                        }    
                    
                    if(tempo==Tempo){
                            break;
                        }
                    tmpsrc = copia->next;
                    
                    free(copia);
                    if(tmpsrc==NULL){
                        printf("\tI am NULL\n");
                        break;
                    }
                    copia = tmpsrc;
                } else {
                /* Verifica se o primeiro processo não possui maior prioridade */
                    printf("\tjob else\n");
                    tmp = maiorprimeiro->next;
                    inicio = tempo;
                    

                    for(int i=inicio,j=tempo+tmp->count; i<j;i++){
                        tmp->count--;
                        tempo++;
                        corre(tmp,&k);k++;
                        
                        if(tmp->count==0){
                            fim = tempo;
                            tmp->fim=fim;
                            printf("\tterminado %i segs\n", fim);
                            }
                        if(tempo==Tempo){
                            fim = Tempo;
                            tmp->fim=fim;
                            printf("\ttempo limite\n");
                            break;
                            }

                    }
                    
                    maiorprimeiro->next = tmp->next;
                    free(tmp);
                    if(maiorprimeiro->next==NULL){
                        printf("\tI am NULL\n");
                        break;
                    }
                }
            }

  }
  printf("fim priotity\n\n");
}

/* Escalonamento SJF*/
void sjf (PCB *proc) {


  int inicio, fim, shortest,k=0;
  PCB *copia, *tmpsrc, *tmp, *beforeshortest;
  printf("\tEscalonamento SJF\n");
  printf("\n");
  /* Lista de processos é replicada */
  tmpsrc = CopyPCB(createHead(),proc);
  copia = tmp = NULL;
  while (tmpsrc != NULL) {
    if (copia == NULL) {
    copia = (PCB *) malloc( sizeof( PCB ));
    copia = CopyPCB(createHead(),tmpsrc);
    tmp = CopyPCB(createHead(),copia);
    } else {
    tmp->next = CopyPCB(createHead(),tmpsrc);
    tmp = tmp->next;
    }
    tmpsrc = tmpsrc->next;
  }
  tempo = 0;
  while (copia != NULL) {
    /* Encontra o nextimo processo*/
    beforeshortest = NULL;
    shortest = copia->count;
    tmp = copia->next;
    tmpsrc = copia;
    while (tmp != NULL) {
        if (tmp->count < shortest) {
        shortest = tmp->count;
        beforeshortest = tmpsrc;
        }
    tmpsrc = tmp;
    tmp = tmp->next;
    }
    /* Executa processo e remove ráplica da lista de processos */
    if (beforeshortest == NULL) {
    /* Aloca o primeiro processo caso o mesmo seja menor */
    inicio = tempo;

for(int i=inicio,j=tempo+copia->count; i<j;i++){
        copia->count--;
        tempo++;
        corre(copia,&k);
        k++;
        if(copia->count==0){
        fim = tempo;
        copia->fim=fim;
        printf("processamento terminado\n");
        }
        if(tempo==Tempo){ 
          fim = Tempo;
          copia->fim=fim;
          printf("tempo limite!\n");
          break;
        } 
    }
    tmpsrc = copia;
    copia = copia->next;
    free(tmpsrc);
    if(tempo==Tempo){ 
          break;
        }
    } else {
    /* Aloca o primeiro processo caso não haja 
    ocorrencia de outro menor 
    */
    tmp = beforeshortest->next;
    inicio = tempo;

    for(int i=inicio,j=tempo+tmp->count; i<j;i++){
        tmp->count--;
        tempo++;
        corre(tmp,&k);
        k++;
      if(tmp->count==0){
        fim = tempo;
        tmp->fim=fim;
        tmp = tmp->next;
        printf("processamento terminado\n");
            if(tmp==NULL)
            {
              break;  
            }
        }
      if(tempo==Tempo){
          fim = Tempo;
          printf("tempo limite\n");
          break;
      }
    }

    beforeshortest->next = tmp->next;
    free(tmp);
    if(tempo==Tempo){
          fim = Tempo;
          printf("tempo limite\n");
          break;
      }
    }
  }
     
printf("fim sjf\n");    
}

void Reportage2(PCB *L, int tip){

    
        if(tip ==1){
        printf("TEMPO ATUAL: %i segs\n\n",tempo);
        printf("PROCESSO EM EXECUÇÃO:\nPid: %i Ppid: %i Prioridade: %i Valor da Variavel: %i Tempo de Iniciação: %i Tempo do CPU usado: %i\n",L->PID,L->PPID,L->priority,L->val,L->Start,L->PC);
        
        }else{
        printf("ENTROU:\n");
        PCB *Ready=infoReady(L);
        printf("block\n");
        PCB *Blocked=infoBlocker(L);
        printf("term\n");
        PCB *Term=infoTerminated(L);
        printf("PROCESS0S BLOQUEADOS:\n");
        
        if(Blocked!=NULL)
            while(Blocked!=NULL){//for para correr o vector pcb para mostrar só os bloqueados
                printf("Pid: %i Ppid: %i Prioridade: %i Valor da Variavel: %i Tempo de Iniciação: %i Tempo do CPU usado: %i\n",Blocked->PID,Blocked->PPID,Blocked->priority,Blocked->val,Blocked->Start,Blocked->fim);
                Blocked=Blocked->next; if(Blocked==NULL) break;          }
        printf("PROCESSOS PRONTOS A EXECUTAR\n");
        
        if(Ready!=NULL)
            while(Ready!=NULL){//for para correr o vector pcb para mostrar só os prontos a executar
                printf("Pid: %i Ppid: %i Prioridade: %i Valor da Variavel: %i Tempo de Iniciação: %i Tempo do CPU usado: %i\n",Ready->PID,Ready->PPID,Ready->priority,Ready->val,Ready->Start,Ready->fim);
                Ready=Ready->next;   if(Ready==NULL) break;                 }
        printf("PROCESSOS TERMINADOS\n");
        
        if(Term!=NULL)
            while(Term!=NULL){//for para correr o vector pcb para mostrar só os terminados
                printf("Pid: %i Ppid: %i Prioridade: %i Valor da Variavel: %i Tempo de Iniciação: %i Tempo do CPU usado: %i\n",Term->PID,Term->PPID,Term->priority,Term->val,Term->Start,Term->fim);
                Term=Term->next;     if(Term==NULL) break;                }
        
        free(Term);
        free(Blocked);
        free(Ready);
        }
}
/*
void swap ( int* a, int* b ) 
{ int t = *a; *a = *b; *b = t; } 
  
// A utility function to find last node of linked list 
PCB *lastProc(PCB *L) 
{ 
    while (L->next!=NULL) 
        L = L->next; 
    return L; 
} 

PCB * partition(PCB *l, PCB *h) 
{ 
    // set pivot as h element 
    int x = h->count; 
  
    // similar to i = l-1 for array implementation 
    PCB *i = l->prev; 
  
    // Similar to "for (int j = l; j <= h- 1; j++)" 
    for (PCB *j = l; j != h; j = j->next) 
    { 
        if (j->count <= x) 
        { 
            // Similar to i++ for array 
            i = (i == NULL) ? l : i->next; 
  
            swap(&(i->count), &(j->count)); 
        } 
    } 
    i = (i == NULL) ? l : i->next; // Similar to i++ 
    swap(&(i->count), &(h->count)); 
    return i; 
} 
  
//A recursive implementation of quicksort for linked list 
void _quickSort(PCB *l, PCB *h) { 
    if (h != NULL && l != h && l != h->next) 
    { 
        PCB *p = partition(l, h); 
        _quickSort(l, p->prev); 
        _quickSort(p->next, h); 
    } 
} 
  
// The main function to sort a linked list. 
// It mainly calls _quickSort() 
void quickSort(PCB *head) 
{ 
    // encontra o ultimo node 
    PCB *cauda = lastProc(head); 
  
    // Call the recursive QuickSort 
    _quickSort(head, cauda); 
} */

PCB * listErase(PCB* L){
    while(L!=NULL){
    L= listErase(L->next);
    free(L);
    }

    return L;  
}

/*int validlength(){
    int k=0;
    for(int i=0;i<1000;i++){
        if(( memory[i].ins != '\0'|| strcmp(memory[i].nome,NULL)!=0) && memory[i].n==0 ){
            k++;
        }else if((memory[i].ins!='\0' || strcmp(memory[i].nome,NULL)!=0) && memory[i].n!=0 ){
            k++;
        }else break;
    }
    return k;
}*/