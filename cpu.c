#include "cpu.h"
#include "processo.h"

void execNextInst(PCB *p, Cpu *cpu, bufferCPU* buffer)
{
    if (cpu->pc >= p->qtdInst) {
        printf("\nErro\n");
        return;
    }
    char *instrucao = p->texto[cpu->pc];
    cpu->variavel = p->variavel;
    
    if (strcmp(p->texto[cpu->pc+1],"T") != 0){
         execInstrucao(p, instrucao, cpu, buffer);
         p->variavel = cpu->variavel;
         }
    else {
        execInstrucao(p, instrucao, cpu, buffer);
        p->variavel = cpu->variavel;
        p->estadoAtual = TERMINADO;
        
        if(p->idPai != 0){  
            buffer->comando = FINISHEDFILHO;
        }
        else 
        buffer->comando = FINISHED;
    }
}

void execInstrucao(PCB *p, char *instrucao, Cpu *cpu, bufferCPU* buffer)
{
    char inst = instrucao[0];
    int n = 0;
    char nomeFicheiro[30] = "";
    printf("Instrução %s\n", instrucao);
    cpu->tempoUsado += 1;
    switch (inst)
    {
        case 'M':   //define valor
            n = getNumber(instrucao); 
            cpu->variavel = n;
            cpu->pc = cpu->pc + 1;
            buffer->comando = ZERO;
            break;
        case 'A':   //soma n
            n = getNumber(instrucao);
            cpu->pc = cpu->pc + 1;
            cpu->variavel +=  n;
            buffer->comando = ZERO;
            break;
        case 'S':   //subtrai n
            n = getNumber(instrucao);
            cpu->pc = cpu->pc + 1;
            cpu->variavel -=  n;
            buffer->comando = ZERO;
            break;
        case 'B':   //bloqueia
            p->estadoAtual = BLOQUEADO;
            cpu->pc = cpu->pc + 1;
            buffer->comando = BLOCK;
            break;
        case 'C':   //cria filho
            n = getNumber(instrucao);
            cpu->pc = cpu->pc + 1;
            buffer->comando = F0RK;
            buffer->n = n;
            break;
        case 'L':   //substitui programa
            buffer->comando = CHANGE;
            cpu->pc = 0;
            cpu->variavel = NULL;
            strncpy(buffer->arquivo, instrucao+2, strlen(instrucao)-3); 
            buffer->arquivo[strlen(buffer->arquivo)] = '\0'; 
            carregaInstrucoes(p, buffer->arquivo);
            break;
        default: printf("ERRO! %s", instrucao);
    }
}

int getNumber(char *t) 
{
    int i, n=0;
    sscanf(t,"%*[^0-9]%d",&n);
    return n;
}
