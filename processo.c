#include <string.h>
#include "processo.h"
#include "gestorprocessos.h"


int criaProcesso(Gestor *g, int id, int prioridade, char *file, int arrivalTime){

    int i;
    PCB *p;
    p = (PCB*) malloc(sizeof(PCB));
    p->qtdInst = 0; 
    p->idPai = 0;  
    p->pc = 0;
    p->id = id;
    p->prioridade = prioridade;
    p->estadoAtual = NOVO;
    p->arrivalTime = arrivalTime;
    p->timeRunning = 0; 
    p->variavel = 0;

    carregaInstrucoes(p, file);

    for (i=1; i<=MAXPROCESS; i++){
        if(g->pcbTabela[i] == NULL){
            g->pcbTabela[i] = p;
            break;
        }
    }
    p->estadoAtual = PRONTO;
    g->contadorID++;
    return i;
}


int criaProcessoFilho(Gestor *g, PCB *pai){

    printf("\nProcesso Filho criado.\n");
    PCB *p;
    int i;
    p = (PCB*) malloc(sizeof(PCB));
    p->qtdInst = pai->qtdInst;
    p->pc = g->cpu.pc;
    p->id = g->contadorID;
    p->prioridade = pai->prioridade;
    p->estadoAtual = RUNNING;
    p->idPai = pai->id;
    p->variavel = 0;
    p->arrivalTime = g->time;
    p->timeRunning = 0;

    for(i=0; i< pai->qtdInst; i++ ) { //copia as instruções do pai para o filho
        //printf("strcpy(%s, %s)\n", p->texto[i], pai->texto[i] );
        strcpy(p->texto[i], pai->texto[i]);
    }

    for (i=1;i<=MAXPROCESS;i++){ //insere o processo em pcbTabela
        if(g->pcbTabela[i] == NULL){
            g->pcbTabela[i] = p;
            break;
        }
    }
    p->estadoAtual = PRONTO;
    insereLista(g, i, 'F');
    g->contadorID ++; 
    //printf("criei filho\n\n\n\n");
    //sleep(1);
    return i;
}
