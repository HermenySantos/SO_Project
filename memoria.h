#ifndef MEMORIA_H_INCLUDED
#define MEMORIA_H_INCLUDED
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#define MEMORYSIZE 256
#define BLOCKSIZE 2
#define NUMREQUESTS 10000



typedef struct Memoria {
	int memoria; 
	int pid;
	struct Memoria *prox;
} Memoria;

Memoria *criaMemoria();
int allocate_firstFit(int process_id, int num_units);
int allocate_bestFit(int process_id, int num_units);
int allocate_worstFit(int process_id, int num_units);
int allocate_nextFit(int process_id, int num_units);
int deallocate_mem(int process_id);
int fragment_count();
void inicializaMemoria();
void reportMemoria();
void simula(int tipo);

#endif